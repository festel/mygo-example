package main

import (
	"flag"
	"fmt"
	"log"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"mygo-example/utils"
	"os"

	"mygo-example/conf"
	"mygo-example/app"
)


func main() {
	runSrvPtr := flag.Bool("run", false, "Run server")
	flag.Parse()

	if flag.NFlag() == 0 {
		fmt.Println("Type `main -help` to show acceptable flags")
		os.Exit(0)
	}

	if flag.NFlag() > 1 {
		fmt.Println("Only 1 flag can be passed")
		os.Exit(0)
	}

	config, err := conf.NewConfig("config.yaml").Load()
	if err != nil {
		log.Fatal(err)
	}

	instance := &app.App{}
	instance.Init(config)

	if *runSrvPtr {
		router := chi.NewRouter()
		router.Use(middleware.Logger)
		router.Route("/api", func(router chi.Router) {
			router.Use(utils.APIHeaders)
			router.Route("/me", func(router chi.Router) {
				router.Use(instance.UserCtx)
				router.Get("/", instance.UserInfo)
			})
			router.Post("/login", instance.Login)
			router.Post("/reg", instance.RegUser)
		})

		instance.Run(router)
	}
}
