package app

import (
	"fmt"
	"log"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/go-chi/chi"


	"mygo-example/conf"
	"mygo-example/model"
)

type App struct {
	DB        model.Datastore
	Conf      *conf.Config
}

func (app *App) Init(config *conf.Config) {
	mysqlBind := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True",
		config.DB.UserName,
		config.DB.UserPassword,
		config.DB.Host,
		config.DB.Port,
		config.DB.Database,
	)

	db := sqlx.MustConnect("mysql", mysqlBind)
	db = db.Unsafe()

	app.Conf = config
	app.DB = &model.DB{db}
}

func (app *App) Run(router *chi.Mux) {
	hostBind := fmt.Sprintf("%s:%s",
		app.Conf.Host.IP,
		app.Conf.Host.Port,
	)
	if app.Conf.Debug{
		fmt.Printf("\033[1;33m%s\033[0m\r\n", "** DEBUG MODE IS ACTIVE! **")
	}

	fmt.Printf("\033[0;32m%s\033[0m\r\n", ">> Here we go! Server is run on "+hostBind)
	log.Fatal(http.ListenAndServe(hostBind, router))
}
